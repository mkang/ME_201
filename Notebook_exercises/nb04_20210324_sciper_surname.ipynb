{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Notebook 4 – Translating tensors to Python\n",
    "\n",
    "#### Today's exercise is an introduction to arrays using `numpy` and plotting using `matplotlib`.\n",
    "\n",
    "#### _Purpose of today's exercise_\n",
    "\n",
    "The goals are to:\n",
    "\n",
    "* calculate with vectors and matrices, using `numpy`\n",
    "* plot functions, using `matplotlib`\n",
    "\n",
    "We will in later notebooks apply this to two important **kinematic tensors** that describe material motion and deformation. You will also be able to use these functions for your final project.\n",
    "\n",
    "* It may be helpful to right-click the notebook tab in JupyterLab and select \"New View for Notebook\" if you want to view the instructions and code cells side-by-side instead of scrolling.\n",
    "\n",
    "#### _Contents_\n",
    "\n",
    "[**Importing packages**](#importing)\n",
    "\n",
    "[**Matrices with numpy**](#matrices)\n",
    "\n",
    "[**Plotting with matplotlib**](#plotting)\n",
    "\n",
    "#### _Submission format_\n",
    "\n",
    "Please submit the result of your exercises by \n",
    "\n",
    "> * Download the notebook to your computer **as a PDF (.pdf) file**.\n",
    "> \n",
    ">   **File > Export notebook as... > Export notebook to PDF**\n",
    "> \n",
    "> * Upload the notebook to the Jupyter Notebook 4 assignment in Moodle.\n",
    "\n",
    "Don't forget to complete the interactive question (1) at the end before submitting.\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='importing'><a>\n",
    "### Importing packages"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the first Jupyter notebook, we used some functions:\n",
    "\n",
    "* `type()`\n",
    "* `str()`\n",
    "* `print()`\n",
    "\n",
    "and defined our own:\n",
    "\n",
    "* `subtract()`\n",
    "* `dist()`\n",
    "\n",
    "The `type()`, `str()`, and `print()` functions are built-in functions. They come defined with Python.\n",
    "\n",
    "However, Python doesn't come with many built-in functions, so we either need to **define our own** or **import packages**.\n",
    "\n",
    "> **package** – a coherent set of Python functions written by others to be imported and used for a particular purpose"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 1:** A review. \n",
    "\n",
    "**a)** What are the types of objects we learned in Python? Refer to **Notebook 1** to refresh your memory.\n",
    "\n",
    "One of the types is a function. Use a few words to say what each function does in the cell below. Refer to **Notebook 1** to refresh your memory.\n",
    "\n",
    "**b)** the `type()` function\n",
    "\n",
    "**c)** the `print()` function\n",
    "\n",
    "**d)** the `dist()` function"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**a)** your answer here\n",
    "\n",
    "**b)** your answer here\n",
    "\n",
    "**c)** your answer here\n",
    "\n",
    "**d)** your answer here\n",
    "\n",
    "----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 2** Look through [this list of Python built-in functions](https://www.w3schools.com/python/python_ref_functions.asp). There are not that many!\n",
    "\n",
    "----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Introducing numpy and matplotlib\n",
    "\n",
    "`numpy` and `matplotlib` are two of the most common **packages** used for scientific computing.\n",
    "\n",
    "The way to import a package is\n",
    "```python\n",
    "import numpy\n",
    "import matplotlib\n",
    "```\n",
    "Best practice is to do all imports at the beginning of a file or notebook, but this time we will do it by section.\n",
    "\n",
    "##### Ways to import\n",
    "In `numpy` there is a function `arange()` that gives a series of numbers in an array. For example\n",
    "\n",
    "```python\n",
    "arange(0, 10, 1)\n",
    "```\n",
    "\n",
    "produces the array (range from 0 to 10 in steps of 1)\n",
    "\n",
    "```python\n",
    "array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])\n",
    "```\n",
    "\n",
    "(It looks a lot like a list – but there are important differences.)\n",
    "\n",
    "The way we import `numpy` affects how we **call** the function. \n",
    "\n",
    "1. **Full package name.** Using \n",
    "```python\n",
    "import numpy\n",
    "``` \n",
    "we write `numpy.arange()` to call the function.\n",
    "\n",
    "2. **No package name.** Using \n",
    "```python\n",
    "from numpy import *\n",
    "```\n",
    "we write `arange()` to call the function.\n",
    "\n",
    "3. **Package nickname.** Using\n",
    "```python\n",
    "import numpy as np\n",
    "```\n",
    "we write `np.arange()` to call the function.\n",
    "\n",
    "There are pros and cons to each version:\n",
    "\n",
    "1. **Cumbersome.** Especially with long package names.\n",
    "\n",
    "2. **Fastest.** Not common practice though – with multiple packages, it can get confusing which functions come from which package. In addition, some packages have the same names for functions.\n",
    "\n",
    "3. **Recommended.** Imports numpy with a \"nickname.\" Less cumbersome than 1, but without the potential confusion of 2."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='matrices'><a>\n",
    "### Matrices\n",
    "#### The numpy package"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 3** \n",
    "\n",
    "**a)** Import numpy in the cell below using the third import method (as \"np\").\n",
    "\n",
    "**b)** Check that it is imported by typing `np` in the next cell. Run it. (Use the small play button or Shift + Enter.)\n",
    "\n",
    "* It is loaded if the result is something like <module 'numpy' from ...location...>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# a\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# b\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 4** \n",
    "\n",
    "Working with arrays. Arrays are similar to lists, as we saw above, but more powerful, because they act more \"naturally\" like vectors and matrices we are used to.\n",
    "\n",
    "**a)** Make a new array from a list `[1, 2, 3]` by wrapping it inside the function `np.array()`. Try `np.array([1, 2, 3])`.\n",
    "\n",
    "**b)** Check the type of list `[1, 2, 3]`.\n",
    "\n",
    "**c)** Check the type of array `np.array([1, 2, 3])`.\n",
    "\n",
    "**d)** Add lists `[1, 2, 3]` and `[4, 5, 6]` using `+`.\n",
    "\n",
    "**e)** Add arrays `np.array([1, 2, 3])` and `np.array([4, 5, 6])` using `+`.\n",
    "\n",
    "Notice how the result of **adding arrays** is the behavior of adding two vectors together, unlike the result of **adding lists**.\n",
    "\n",
    "**Observe some other vector behavior**\n",
    "\n",
    "**f)** Add the **array** `np.array([1, 2, 3])` to the **integer** `1`. Try adding a **list** to the integer 1 if you wish and you will see an error.\n",
    "\n",
    "**g)** We can also do a **dot product of arrays**, not possible with a **list**. Find the dot product of the array `np.array([1, 2, 3])` with itself using\n",
    "\n",
    "`\n",
    "np.array([1, 2, 3]).dot(np.array([1, 2, 3]))\n",
    "`\n",
    "\n",
    "**Matrices and vectors**\n",
    "\n",
    "**h)** To avoid repeatedly writing the same array, define an array `a` and matrices `M` and `I` like below.\n",
    "\n",
    "`a = np.array([1, 2, 3])`\n",
    "\n",
    "`b = np.array([0, 1, 1])`\n",
    "\n",
    "`M = np.array([[2, 0, 0], [3, 4, 0], [5, 6, 7]])`\n",
    "\n",
    "`I = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])`\n",
    "\n",
    "For **matrices**, rows are written as separate lists, one after another, separated by commas. This means an **array** like `a` is a column vector (`a` has 3 rows here). `I` is done for you, to show how using multiple lines makes it read more like a matrix.\n",
    "\n",
    "_Hint:_ Remember, as soon as you define a variable once, it stays defined throughout all code cells in a Jupyter notebook. For the next exercises, no need to redefine `a`, `b`, `M`, and `I`.\n",
    "\n",
    "**i)** Multiply `a` by identity matrix `I` using `I.dot(a)`.\n",
    "\n",
    "**j)** Multiply `a` by matrix `M` using `M.dot(a)`.\n",
    "\n",
    "**k)** Transpose matrix `M` using `M.transpose()`.\n",
    "\n",
    "**l)** Transpose matrix `M` a different way using `M.T`.\n",
    "\n",
    "**m)** Multiply matrices `I` and `M` together using `M.dot(I)`.\n",
    "\n",
    "**n)** Multiply the matrix `M` with itself.\n",
    "\n",
    "**o)** **Dot product.** Try another way. Use `np.dot()` to multiply `a` by `M` – `np.dot(M, a)`. Note that this is not the same as calculating `np.dot(a, M)` (just like in linear algebra). Try both in the same cell.\n",
    "\n",
    "_Hint:_ If you have multiple calculations in the same cell, you need to `print()` the results. Otherwise, only the last calculation result shows when running a Jupyter cell.\n",
    "\n",
    "**p)** **Cross product.** Use `np.cross()` to find the cross product of `a` and `b`.\n",
    "\n",
    "**q)** **Multiplication by a scalar.** In the same cell, separate lines, multiply `a`, `b`, `I`, and `M` by `2`.\n",
    "\n",
    "_Hint:_ Just like above, if you have multiple calculations in the same cell, you need to `print()` the results.\n",
    "\n",
    "**r)** It is also possible to square terms element-wise for arrays. Not possible with lists. Square each term in `a` using `a ** 2`. In the same cell, take the square root of each term in `a` using `a ** (1/2)`.\n",
    "\n",
    "**s)** Your turn. Define 2 or more vectors and matrices of your own, of any size. Try using one of the operations above.\n",
    "\n",
    "**t)** Your turn. Define 2 or more vectors and matrices of your own, of any size. Try using a different one of the operations.\n",
    "\n",
    "**u)** Your turn. Define 2 or more vectors and matrices of your own, of any size. Try using a different one of the operations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "metadata": {},
   "outputs": [],
   "source": [
    "# a\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# b\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# c\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# d\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# e\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "# f\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "metadata": {},
   "outputs": [],
   "source": [
    "# g\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "# h\n",
    "a =\n",
    "b = \n",
    "M = \n",
    "I = np.array([[1, 0, 0],\n",
    "              [0, 1, 0],\n",
    "              [0, 0, 1]])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# i\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# j\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# k\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# l\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 46,
   "metadata": {},
   "outputs": [],
   "source": [
    "# m\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# n\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# o\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# p\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# q\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# r\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# s\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# t\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# u\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 5**\n",
    "\n",
    "The way to retrieve an element from a `numpy` **array** is called **indexing**. Indexing in Python starts from `0`, so the first element in an array is labeled `0`, the second element in an array is labeled `1`, the third element in an array is labeled `2`, etc.\n",
    "\n",
    "...Yes, this is confusing, we just cannot change it. So in continuum mechanics our indices take the values $i = 1, 2, 3$ and in Python the corresponding indices are `0`, `1`, `2`.\n",
    "\n",
    "**a)** Using the vector `a` from **Exercise 4**, try the code `a[0]`.\n",
    "\n",
    "**b)** Use what you learned from **a)** to retrieve the second element of `a`.\n",
    "\n",
    "**c)** Use what you learned from **a)** to retrieve the third element of `a`.\n",
    "\n",
    "**d)** What happens when you try `a[3]`?\n",
    "\n",
    "**e)** Using the matrix `M` above, try the code `M[0]`.\n",
    "\n",
    "**f)** Using the `M` above, try the code `M[0][0]`.\n",
    "\n",
    "**g)** Do the same thing using a slightly different notation, with a comma between row and column `M[0, 0]`.\n",
    "\n",
    "**h)** Use what you learned to retrieve the $M_{23}$ component of `M`.\n",
    "\n",
    "**i)** Use what you learned to retrieve the $M_{33}$ component of `M`.\n",
    "\n",
    "**j)** Your turn. Try retrieving a component of any vectors or matrices you defined in **Exercise 4**.\n",
    "\n",
    "**k)** Your turn. Try retrieving a component of any vectors or matrices you defined in **Exercise 4**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [],
   "source": [
    "# a\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# b\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# c\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# d\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# e\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "# f\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "# g\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# h\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# i\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# j\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# k\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 6**\n",
    "\n",
    "We can use functions on arrays. \n",
    "\n",
    "**Use these functions on array `a` and matrix `M`.**\n",
    "\n",
    "**a)** `np.sin()`, `np.cos()`, `np.tan()`\n",
    "\n",
    "Example with `np.sin()` done for you.\n",
    "\n",
    "**b)** `np.sqrt()`\n",
    "\n",
    "**Use these functions each on `a`, `b`, `M`, and `I`.**\n",
    "\n",
    "**c)** `np.sum()`\n",
    "\n",
    "Sums all components.\n",
    "\n",
    "Using the objects you already defined from **Exercise 4** –\n",
    "\n",
    "Find the sum of `a`, \n",
    "find the sum of `b`, \n",
    "find the sum of `M`, and \n",
    "find the sum of `I`.\n",
    "\n",
    "**d)** `np.min()`, `np.max()`\n",
    "\n",
    "Finds the min or max of all components.\n",
    "\n",
    "Find the **min** and **max** of each array `a`, `b`, `M`, and `I`.\n",
    "\n",
    "**Some important numpy functions.**\n",
    "\n",
    "**e)** `np.arange()`\n",
    "\n",
    "This function generates a sequential array (handy for not typing 100-component vectors!), given the arguments `np.arange(start, stop, step)`, where\n",
    "\n",
    "* `start` = value to start array at (inclusive)\n",
    "* `stop` = value to end array at (exclusive)\n",
    "* `step` = step size between values\n",
    "\n",
    "**In the same cell:**\n",
    "\n",
    "Generate an array from 0 to 12 (excluding 12) with step size 1. Use `print()` to see the result.\n",
    "\n",
    "Generate an array from 0 to 22 (including 22) with step size 2. For this, the second argument has to be above `22`, so we usually write `np.arange(0, 22+1, 2)`. Print it.\n",
    "\n",
    "Generate an array from 0 to 1 (excluding 1) with step size 0.05. Print it.\n",
    "\n",
    "**f)** `np.linspace()`\n",
    "\n",
    "This function also generates a sequential array. This time you specify `np.linspace(start, stop, num, endpoint=True)`, where\n",
    "\n",
    "* `start` = value to start array at (inclusive)\n",
    "* `stop` = value to stop array at (exclusive, unless `endpoint=True`)\n",
    "* `num` = numer of values between start and stop\n",
    "* `endpoint` = `True` (default) or `False`, whether to include the `stop` value\n",
    "\n",
    "**What is an optional argument?** An optional argument has the form `arg=defaultvalue` in a function. In `np.linspace()` if you simply write `np.linspace(start=0, stop=50, step=5)` and don't specify the argument `endpoint`, it assumes the default value `True` and the array will stop at `50`. If `False`, it stops at `45`.\n",
    "\n",
    "**In the same cell:**\n",
    "\n",
    "Generate an array from 1 to 12 (including 12) with 12 values. Use `print()` to see the result.\n",
    "\n",
    "Generate an array from 1 to 20 (excluding 20) with 5 values. Print it.\n",
    "\n",
    "Generate an array from 0 to 1 (including 1) with 40 values. Print it.\n",
    "\n",
    "**g)** `np.round()`\n",
    "\n",
    "Rounds numbers, with input `np.round(num, decimals)`.\n",
    "\n",
    "Round `3.1415` to 2 decimal places.\n",
    "\n",
    "Round `607.403` to 1 decimal place.\n",
    "\n",
    "Round `4196` to the hundreds place. Use `decimals=-2`.\n",
    "\n",
    "----"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[0.84147098 0.90929743 0.14112001]\n",
      "[[ 0.90929743  0.          0.        ]\n",
      " [ 0.14112001 -0.7568025   0.        ]\n",
      " [-0.95892427 -0.2794155   0.6569866 ]]\n"
     ]
    }
   ],
   "source": [
    "# a\n",
    "print(np.sin(a))\n",
    "print(np.sin(M))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# b\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 60,
   "metadata": {},
   "outputs": [],
   "source": [
    "# c\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# d\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# e\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 61,
   "metadata": {},
   "outputs": [],
   "source": [
    "# f\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 62,
   "metadata": {},
   "outputs": [],
   "source": [
    "# g\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 7**\n",
    "\n",
    "Take a brief look at some other functions of `numpy` in this [list of numpy mathematical functions](https://numpy.org/doc/stable/reference/routines.math.html).\n",
    "\n",
    "----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 8**\n",
    "\n",
    "`numpy` can be used to calculate indicial notation!\n",
    "\n",
    "[Here is the description](https://numpy.org/doc/stable/reference/generated/numpy.einsum.html) for `numpy`'s einsum function.\n",
    "\n",
    "Note that the first argument of the `np.einsum()` function is a **string** (such as `'ij'` or `'ij->ji'` related to the indices of an **array**. The **array** or **arrays** to sum, transpose, etc. are the next arguments.\n",
    "\n",
    "Using `M` and `a` from **Exercise 4**:\n",
    "\n",
    "**a)** Compute the **transpose** of `M` using `np.einsum('ij->ji', M)`. This signifies the indicial notation where $\\mathbf{M} = [M_{ij}]$ and its transpose is thus $\\mathbf{M}^T = [M_{ji}]$.\n",
    "\n",
    "**b)** Since `np.einsum()` assumes the indices of $\\mathbf{M}$ are originally in alphabetical order (e.g. $ij$), the above can also be done 'implicitly' simply by `np.einsum('ji', M)`. Try it.\n",
    "\n",
    "**c)** Compute the **diagonal components** of `M` using `np.einsum('ii->i', M)`.\n",
    "\n",
    "**d)** Compute the **inner product** of vectors `a` and `b` from **Exercise 4** using `np.einsum('i,i', a, b)`.\n",
    "\n",
    "**e)** How is part **d)** written in indicial notation? Please use LaTex to express it.\n",
    "\n",
    "**f)** Based on part **d)**, how would you find the result of matrix `M` multiplied by vector `a`? Calculate with the `np.einsum()` function.\n",
    "\n",
    "_Hint:_ Write it in indicial notation. Then convert that to the first argument of `einsum`.\n",
    "\n",
    "----"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# a\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# b\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# c\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# d\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**e)**\n",
    "\n",
    "...your answer here..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# f\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 9**\n",
    "\n",
    "**a)** In our discussion of the scalar invariants for a tensor, we saw that we can concisely express them using indicial notation. First define the tensor:\n",
    "$$ [\\mathbf{T}] = \\begin{bmatrix} 2 & 0 & 0 \\\\ 0 & 3 & 4 \\\\ 0 & 4 & -3 \\end{bmatrix} $$\n",
    "Then use `np.einsum()` to compute its first two scalar invariants.\n",
    "\n",
    "As a reminder, the first two invariants are written in indicial notation as:\n",
    "\n",
    "$$ I_1 = T_{ii} $$ and $$  I_2 = \\frac{1}{2} \\left(T_{ii}T_{jj} - T_{ij}T_{ji}\\right) $$\n",
    "\n",
    "Note that $T_{ij}T_{ji}$ is a rank 0 tensor (scalar) which can be readily calculated by `np.einsum('ij, ji', T, T)`. This is double dot product. Here the tensor $\\mathbf{T}$ is double producted by itself. Refer to Handout 1 (Appendix A) in Moodle.\n",
    "\n",
    "**b)** We know that the third invariant is given by the determinant of the tensor $\\mathbf{T}$. This can be calculated from a tool in the `linalg` sub-module of `numpy`. You can access this sub-module by appending `.linalg` after `np`, and use the function `np.linalg.det()` to calculate the third invariant of $\\mathbf{T}$. \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# a\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# b\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Creating a user-defined function to compute eigenvalues\n",
    "\n",
    "Further use `np.linalg.eigvals()` to compute the eigenvalues. Here we calculate the values for the same tensor $\\mathbf{T}$. From the class notes and the Handout 1 we know that the eigenvalues should satisfy the characteristic equation for $\\mathbf{T}$:\n",
    "$$\n",
    "\\lambda^3 - I_1 \\lambda^2 + I_2 \\lambda - I_3 = 0\n",
    "$$\n",
    "\n",
    "**Run the next 2 cells.** Recall how we define a function in Python (see the end of Notebook 1). Especially mind the tab from the second line onwards. Our user-defined function `characteristic_polynomial` calculates the left hand side of the equation above (polynomial).\n",
    "\n",
    "Evaluation of a user-defined function in Python is very easy. We can simply evaluate it by `result=characteristic_polynomial(T, lambdas[0])` by giving it 2 varables as the input arguments. \n",
    "\n",
    "**c)** Call (evaluate) and print the value of the function output for the matrix `T` and each separate eigenvalue `lambdas[0]`, `lambdas[1]`, `lambdas[2]`. Is the output of the function zero as expected?\n",
    "\n",
    "----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Run the following 2 cells, no need to edit them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[-5.  5.  2.]\n",
      "<class 'numpy.ndarray'>\n"
     ]
    }
   ],
   "source": [
    "T = np.array([[2, 0, 0],\n",
    "              [0, 3, 4],\n",
    "              [0, 4, -3]])\n",
    "lambdas=np.array(np.linalg.eigvals(T))  # This numpy function calculates the eigenvalues\n",
    "print(lambdas)\n",
    "print(type(lambdas))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def characteristic_polynomial(input_T, input_scalar):\n",
    "    TijTji=np.einsum('ij,ji->',input_T,input_T)\n",
    "    I1=np.einsum('ii->', input_T) \n",
    "    I2=0.5*(I1*I1-TijTji)\n",
    "    I3=np.linalg.det(input_T)\n",
    "    lhs = input_scalar**3 - I1 * input_scalar**2 + I2 * input_scalar - I3\n",
    "    return lhs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "# c\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='plotting'><a>\n",
    "### Plotting\n",
    "#### The matplotlib package"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 10**\n",
    "\n",
    "**a)** Import `matplotlib.pyplot` in the code cell below, using the third import method with its commonly used short name `plt`.\n",
    "\n",
    "**b)** Check that it is imported by typing `plt` in the second cell below. Run it.\n",
    "\n",
    "* It is loaded if the result is something like <module 'matplotlib.pyplot' from ...location...>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# a\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# b\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 11**\n",
    "\n",
    "Time for a first plot!\n",
    "\n",
    "* This exercise is a good reference if you choose to plot data for your final project.\n",
    "\n",
    "**a)** Generate data. Make an array `x` of points from 0 to 10 (doesn't matter whether endpoint included) with 1000 points. Make an array `y1` equal to the cosine of `x`. Make an array `y2` equal to the sine of `x`. Make an array `y3` equal to `np.sin(x + np.pi/2) + 5`. Example `y1` is done for you.\n",
    "\n",
    "**b)** Use the steps [in this tutorial](https://vegibit.com/matplotlib-in-jupyter-notebook/) under `plt.plot()` to plot these functions `y1`, `y2`, and `y3` in one graph with appropriate **title**, **axis labels**, and **legend** that you choose.\n",
    "\n",
    "_Hint:_ Don't forget `plt.show()` **at the end** of the plotting code.\n",
    "\n",
    "**c)** You'll see [in this tutorial](https://vegibit.com/matplotlib-in-jupyter-notebook/) they use `plt.scatter()` to plot points. We will not do this, since a scatterplot can be made using the `plt.plot()` function too. \n",
    "\n",
    "Use the given arrays `data_x` and `data_y` to plot points using \n",
    "\n",
    "`plt.plot(data_x, data_y, linestyle='', marker='o', markersize=2, color='red', label='tensile test data')`. \n",
    "\n",
    "What does each part mean?\n",
    "\n",
    "> `linestyle=''` turns off the line between points, \n",
    "> \n",
    "> `marker='o'` specifies using a round marker (as opposed to `+`, `x`, etc.) \n",
    "> \n",
    "> `markersize=2` specifies the point size and \n",
    "> \n",
    "> `color='red'` allows us to choose the color. \n",
    ">\n",
    "> `label='tensile test data'` allows us to label the data as they'll appear in the legend.\n",
    "\n",
    "Don't forget to add an appropriate **title**, **axis labels**, and **legend**, based on the fact that this data is strain (x-axis, in %) versus stress (y-axis, in MPa) for a tensile test of a nanocomposite film. You don't require more information than that, but in case you are curious, here is a link to the [tensile test dataset](http://doi.org/10.1038/s41598-017-17647-w).\n",
    "\n",
    "_Hint:_ Don't forget `plt.show()` **at the end** of the plotting code.\n",
    "\n",
    "**d)** Styling. Look once more [in this tutorial](https://vegibit.com/matplotlib-in-jupyter-notebook/) under \"Matplotlib styles.\" Either generate your own data using a function or use the data from part **c)**, make a plot, and give it a style of your choosing. This time, **before all the plotting code**, use\n",
    "\n",
    "`plt.figure(figsize=(x_size, y_size))` – for example `plt.figure(figsize=(10, 4))`\n",
    "\n",
    "to specify the plot size you want (in US inches, ~2.2 cm).\n",
    "\n",
    "_Note:_ You do _not_ need to load a CSV file like in the tutoral. Just pay attention to the code for adding **styles**.\n",
    "\n",
    "----"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# a\n",
    "x = # Define x here\n",
    "y1 = np.cos(x)\n",
    "y2 = # Define y2 here\n",
    "y3 = # Define y3 here"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# b\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "# c\n",
    "data_x = np.array([0.   ,   4.294,   8.588,   12.88,   17.18,   21.47,   25.76,   30.06,   34.35,\n",
    "                   38.65,   42.94,   47.23,   51.53,   55.82,   60.12,   64.41,   68.7 ,   73.  ,\n",
    "                   77.29,   81.59,   85.88,   90.17,   94.47,   98.76,   103.1,  107.3 ,  111.6 ,\n",
    "                   115.9,   120.2,   124.5,   128.8,   133.1,   137.4,   141.7,   146. ,   150.3,\n",
    "                   154.6,   158.9,   163.2,   167.5,   171.8,   176.1,   180.3,   184.6,   188.9,\n",
    "                   193.2,   197.5,   201.8,   206.1,   210.4,   214.7,   219. ,   223.3,   227.6,\n",
    "                   231.9,   236.2,   240.5,   244.8,   249.1,   253.3,   257.6,   261.9,   266.2,\n",
    "                   270.5,   274.8,   279.1,   283.4,   287.7,   292. ,   296.3,   300.6,   304.9,\n",
    "                   309.2,   313.5,   317.8,   322. ,   326.3,   330.6,   334.9,   339.2,   343.5,\n",
    "                   347.8,   352.1,   356.4,   360.7,   365. ,   369.3,   373.6,   377.9,   382.2,\n",
    "                   386.5,   390.8,   395. ,   399.3,   403.6,   407.9,   412.2,   416.5,   420.8,\n",
    "                   425.1,   429.4,   433.7,   438. ,   442.3,   446.6,   450.9,   455.2,   459.5,\n",
    "                   463.8,   468. ,   472.3,   476.6,   480.9,   485.2,   489.5,   493.8,   498.1,\n",
    "                   502.4,   506.7,   511. ,   515.3,   519.6,   523.9,   528.2,   532.5,   536.7,\n",
    "                   541. ,   545.3,   549.6,   553.9,   558.2,   562.5,   566.8,   571.1,   575.4,\n",
    "                   579.7,   584. ,   588.3,   592.6,   596.9,   601.2,   605.5,   609.7,   614. ,\n",
    "                   618.3,   622.6,   626.9,   631.2,   635.5,   639.8,   644.1,   648.4,   652.7,\n",
    "                   657. ,   661.3,   665.6,   669.9,   674.2,   678.5,   682.7,   687. ,   691.3,\n",
    "                   695.6,   699.9,   704.2,   708.5,   712.8,   717.1,   721.4,   725.7,   730.   ])\n",
    "data_y = np.array([0.    ,  0.1223,  0.254 ,  0.348,   0.4045,  0.4986,  0.5926,  0.6585,  0.7243, \n",
    "                   0.7525,  0.8184,  0.8466,  0.9124,  0.9407,  0.9689,  1.007,   1.035,   1.101,  \n",
    "                   1.101,   1.129,   1.157,   1.195,   1.195,   1.223,   1.251,   1.289,   1.289,  \n",
    "                   1.289,   1.317,   1.345,   1.345,   1.345,   1.383,   1.383,   1.411,   1.411,  \n",
    "                   1.411,   1.439,   1.439,   1.477,   1.477,   1.477,   1.505,   1.505,   1.505,  \n",
    "                   1.505,   1.533,   1.533,   1.533,   1.571,   1.571,   1.571,   1.599,   1.599,  \n",
    "                   1.599,   1.627,   1.627,   1.627,   1.665,   1.665,   1.665,   1.693,   1.693,  \n",
    "                   1.721,   1.721,   1.721,   1.759,   1.787,   1.787,   1.787,   1.815,   1.815,  \n",
    "                   1.853,   1.853,   1.881,   1.881,   1.881,   1.91 ,   1.91 ,   1.947,   1.947,  \n",
    "                   1.975,   1.975,   2.004,   2.004,   2.004,   2.041,   2.041,   2.069,   2.098,\n",
    "                   2.098,   2.116,   2.135,   2.164,   2.173,   2.192,   2.22 ,   2.229,   2.267,  \n",
    "                   2.286,   2.314,   2.323,   2.333,   2.37 ,   2.38 ,   2.417,   2.446,   2.465,  \n",
    "                   2.502,   2.54 ,   2.549,   2.587,   2.606,   2.634,   2.7  ,   2.709,   2.747,  \n",
    "                   2.794,   2.831,   2.869,   2.907,   2.944,   2.991,   3.057,   3.095,   3.132,  \n",
    "                   3.179,   3.217,   3.255,   3.321,   3.368,   3.405,   3.443,   3.509,   3.556,  \n",
    "                   3.612,   3.659,   3.697,   3.744,   3.8  ,   3.847,   3.857,   3.894,   3.951,  \n",
    "                   4.007,   4.045,   4.12 ,   4.158,   4.195,   4.261,   4.308,   4.374,   4.44,   \n",
    "                   4.487,   4.562,   4.647,   4.694,   4.769,   4.816,   4.891,   4.948,   5.014,  \n",
    "                   5.098,   5.174,   5.249,   5.334,   5.409,   5.493,   5.569,   5.644,   5.757 ])\n",
    "\n",
    "###################################\n",
    "### ADD YOUR PLOTTING CODE BELOW ##\n",
    "###################################\n",
    "\n",
    "# code here"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "# d\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Interactive questions\n",
    "\n",
    "##### _Answer before submitting notebook to Moodle_ \n",
    "\n",
    "Run the cell below. If you don't see the questions, please make sure you are logged into both Moodle and Noto in the same browser. (May not work with private browsing.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "\n",
       "        <iframe\n",
       "            width=\"800\"\n",
       "            height=\"500\"\n",
       "            src=\"https://moodle.epfl.ch/mod/hvp/embed.php?id=1142437\"\n",
       "            frameborder=\"0\"\n",
       "            allowfullscreen\n",
       "        ></iframe>\n",
       "        "
      ],
      "text/plain": [
       "<IPython.lib.display.IFrame at 0x7f5f95f6e438>"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from IPython.display import IFrame\n",
    "IFrame('https://moodle.epfl.ch/mod/hvp/embed.php?id=1142437', 800, 500)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### End of notebook"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
